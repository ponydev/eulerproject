import org.apache.commons.math3.util.CombinatoricsUtils;

/**
 * Created by killerforfun on 15.03.15.
 */
public class Problem15 {

    public static void main(String[] args) {
        //We can use that number of paths for n rows and m columns is binomial coefficient for m from n so
        int n = 40;
        int m = 20; //n / 2
        System.out.println(CombinatoricsUtils.binomialCoefficient(n, m));

        //Result 2 using that combinates from perfect square o(n) solution
        long result = 1;
        n = 20;
        for (int i = 1; i < n + 1; i++)
            result = result * (n + i)/i;
        System.out.println(result);

    }

}
