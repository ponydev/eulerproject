/**
 * Created by killerforfun on 15.03.15.
 */

public class Problem67 {
    //Using Problem18
    public static void main(String[] args) {
        Problem18 p = new Problem18();
        int[][] a = p.readIntTriangle(100, "input");
        System.out.println(p.simplifyTriangle(a, 100)[0][0]);
    }
}
