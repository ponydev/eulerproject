import org.apache.commons.math3.util.CombinatoricsUtils;

import java.math.BigInteger;

/**
 * Created by killerforfun on 15.03.15.
 */
public class Problem20 {

    public static void main(String[] args) {
        long number = 100;
        System.out.println("Answer: " + sumOfDigits(factorial(number)));
    }

    public static BigInteger factorial (long n) {
        if (n == 1)
            return BigInteger.ONE;
        else
            return BigInteger.valueOf(n).multiply(factorial(n - 1));
    }

    public static long sumOfDigits(BigInteger s) {
        String stringRepresent = s.toString();
        long sum = 0;
        for (int i = 0; i < stringRepresent.length(); i++) {
            sum += Character.getNumericValue(stringRepresent.charAt(i));
        }
        return sum;
    }
}
