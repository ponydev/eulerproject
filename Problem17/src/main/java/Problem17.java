import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by killerforfun on 15.03.15.
 */
public class Problem17 {
    public static HashMap<Integer, Integer> dict = new HashMap();


    public static void main(String[] args) {
        int sum = 0;
        dict.put(0, 3);
        dict.put(1, 3);
        dict.put(2, 3);
        dict.put(3, 5);
        dict.put(4, 4);
        dict.put(5, 4);
        dict.put(6, 3);
        dict.put(7, 5);
        dict.put(8, 5);
        dict.put(9, 4);
        dict.put(10, 3);
        dict.put(11, 6);
        dict.put(12, 6);
        dict.put(13, 8);
        dict.put(14, 8);
        dict.put(15, 7);
        dict.put(16, 7);
        dict.put(17, 9);
        dict.put(18, 8);
        dict.put(19, 8);
        dict.put(20, 6);
        dict.put(30, 6);
        dict.put(40, 5);
        dict.put(50, 5);
        dict.put(60, 5);
        dict.put(70, 7);
        dict.put(80, 6);
        dict.put(90, 6);
        dict.put(100, 10);
        dict.put(1000, 11);

        int endNumber = 1000;

        System.out.println(count(115));

        for (int i = 1;  i < endNumber + 1; i++)
            sum += count(i);
        System.out.println(sum);
    }

    public static int count(int number) {
        if (dict.containsKey(number)) {
            return dict.get(number);
        } else {
            if (number < 100)
                return dict.get(number / 10 * 10) + count(number % 10);
            else
                if (number % 100 == 0)
                    return dict.get(number / 100) + "hundred".length();
                else
                    return dict.get(number / 100) + "hundred".length() + "and".length() + count(number % 100);
        }
    }

}
