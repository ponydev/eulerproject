import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Created by killerforfun on 15.03.15.
 */
public class Problem13 {
    public static void main(String[] args) {
        Problem13 problem13 = new Problem13();
        BigInteger answer = problem13.readAndSum50BigInts("input");
        System.out.println(answer.toString().substring(0, 10));
    }

    public BigInteger readAndSum50BigInts(String inputFileName) {
        Path inputFile = Paths.get(getClass().getClassLoader().getResource(inputFileName).getFile());
        try {
            Scanner input = new Scanner(inputFile.toFile());
            input.close();
            BigInteger sum = new BigInteger("0");

            input = new Scanner(inputFile.toFile());
            while (input.hasNextBigInteger()) {
                sum = sum.add(input.nextBigInteger());
            }
            System.out.println("Read file: " + inputFile.toAbsolutePath());
            return sum;
        } catch (FileNotFoundException e) {
            System.out.println("No file found in: " + inputFile.toAbsolutePath());
            return null;
        }
    }


}
