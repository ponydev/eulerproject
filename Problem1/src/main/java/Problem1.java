import org.apache.log4j.Logger;

/**
 * Created by varkhipov on 5/18/2016.
 */
public class Problem1 {
    public static Logger logger = Logger.getLogger(Problem1.class);

    public static void main(String[] args) {
        int sum = 0;
        for (int i = 1; i < 100; i++) {
            if ((i % 3 == 0) || (i % 5 == 0)) sum += i;
        }
        System.out.println(sum);
    }
}
