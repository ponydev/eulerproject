/**
 * Created by killerforfun on 10.03.15.
 */
public class Problem12 {

    public static int countNumberOfDivisors(long number) {
        int i = 1;
        int count = 1;
        while (i <= Math.sqrt(number)) {
            if (number % i == 0) {
                if (number / i == i) {
                    count++;
                } else {
                    count += 2;
                }
            }

            i++;
        }
        return count;
    }

    public static long findNthTriangleValue(long n) {
        return n + (n - 1) * n / 2;
    }

    public static void main(String[] args) {

//        System.out.println(countNumberOfDivisors(findNthTriangleValue(12376)));

        boolean lessThen500 = true;
        int k = 0;
        long triangleValue = 0;
        long numberOfDivisors = 0;
        while (lessThen500) {
            triangleValue = findNthTriangleValue(k);
            numberOfDivisors = countNumberOfDivisors(triangleValue);
            lessThen500 = numberOfDivisors <= 500;
            k++;
        }
        System.out.println("k = " + k + " divisors: " + numberOfDivisors + " value: " + triangleValue);
    }


}
