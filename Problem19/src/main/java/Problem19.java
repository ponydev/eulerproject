import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by killerforfun on 15.03.15.
 */
public class Problem19 {
    public static HashMap<Integer, Integer> dict = new HashMap<>();
    public static Calendar c = Calendar.getInstance();

    public static void main(String[] args) {
        int currentDayOfWeek = 1;

        int firstDayOfMonthIsSunday = 0;

        dict.put(1, 31);
        dict.put(2, 28);
        dict.put(3, 31);
        dict.put(4, 30);
        dict.put(5, 31);
        dict.put(6, 30);
        dict.put(7, 31);
        dict.put(8, 31);
        dict.put(9, 30);
        dict.put(10, 31);
        dict.put(11, 30);
        dict.put(12, 31);
        dict.put(13, 29);

        int number = 0;

        currentDayOfWeek = 2;
        // 1 january 1900 - monday
        for (int i = 1901; i < 2001; i++) {
            for (int j = 1; j <= 12; j++) {
                currentDayOfWeek = (currentDayOfWeek + getDaysInMonth(j, i)) % 7;
                if (currentDayOfWeek == 0) number++;
            }
        }
        System.out.println(number);
        number = 0;
        for (int i = 1901; i < 2001; i++) {
            for (int j = 1; j <= 12; j++) {
                currentDayOfWeek = getCurrentDayOfWeek(1, j, i);
                if (currentDayOfWeek == 0) number++;
            }
        }

        System.out.println(number);


//
//        for (int i = 1901; i < 2001; i++) {
//            //Century
//            if (i % 100 == 0)
//                isLeapYear = i % 400 == 0;
//            else
//                isLeapYear = i % 4 == 0;
//            for (int j = 1; j <= 12; j++) {
//
//            };
//
//        }
    }


    public static int getCurrentDayOfWeek(int day, int month, int year) {
        int dayOfWeek = 0;
        try {
            c.setTime(new SimpleDateFormat("dd/M/yyyy").parse(day + "/" + month + "/" + year));
            dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (dayOfWeek - 1) % 7;
    }


    public static int getDaysInMonth(int month, int year) {
        boolean isLeapYear = false;
        //Century
        if (year % 100 == 0)
            isLeapYear = year % 400 == 0;
        else
            isLeapYear = year % 4 == 0;
        if (month == 2 && isLeapYear)
            return dict.get(13);
        else
            return dict.get(month);

    }
}
