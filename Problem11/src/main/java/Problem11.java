import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by killerforfun on 10.03.15.
 */
public class Problem11 {
    public static Logger logger = Logger.getLogger(Problem11.class);

    public static void main(String[] args) {
        int rows = 20;
        int columns = 20;
        int numberOfProductNumbers = 4;
        Problem11 m = new Problem11();
        double[][] matrix = m.readIntMatrix(rows, columns, "input");
        RealMatrix rm = MatrixUtils.createRealMatrix(matrix);
        double maximumProduct = 0;

        for (int i = 0; i < columns; i++) {
            maximumProduct = Math.max(maximumProduct, m.maximumArrayProduct(rm.getColumn(i), numberOfProductNumbers));
        }

        for (int i = 0; i < rows; i++) {
            maximumProduct = Math.max(maximumProduct, m.maximumArrayProduct(rm.getRow(i), numberOfProductNumbers));
        }


        //diagonals
        for (int i = numberOfProductNumbers - 1; i < rows; i++) {
            for (int j = 0; j < columns - numberOfProductNumbers + 1; j ++) {
                maximumProduct = Math.max(maximumProduct, rm.getEntry(i, j) * rm.getEntry(i - 1, j + 1) * rm.getEntry(i - 2, j + 2) * rm.getEntry(i - 3, j + 3));
            }
        }
        //reversed diagonals
        for (int i = 0; i < rows - numberOfProductNumbers + 1; i++) {
            for (int j = 0; j < columns - numberOfProductNumbers + 1; j ++) {
                maximumProduct = Math.max(maximumProduct, rm.getEntry(i, j) * rm.getEntry(i + 1, j + 1) * rm.getEntry(i + 2, j + 2) * rm.getEntry(i + 3, j + 3));
            }
        }
        System.out.printf("maximumProduct: %.0f\n", maximumProduct);

    }

    public double maximumArrayProduct(double[] a, int n) {
        double maxProduct = 0;
        for (int i = 0; i < a.length - a.length % n; i += n)
            maxProduct = Math.max(maxProduct, a[i] * a[i + 1] * a[i + 2]);
        return maxProduct;
    }

    public double[][] readIntMatrix(int rows, int columns, String inputFileName) {
        Path inputFile = Paths.get(getClass().getClassLoader().getResource(inputFileName).getFile());
        try {
            Scanner input = new Scanner(inputFile.toFile());

            double[][] a = new double[rows][columns];

            input.close();

            input = new Scanner(inputFile.toFile());
            for (int i = 0; i < rows; ++i) {
                for (int j = 0; j < columns; ++j) {
                    if (input.hasNextInt()) {
                        a[i][j] = input.nextInt();
                    }
                }
            }
            logger.info("Read file: " + inputFile.toAbsolutePath());
            return a;
        } catch (FileNotFoundException e) {
            logger.error("No file found in: " + inputFile.toAbsolutePath());
            return null;
        }
    }
}
