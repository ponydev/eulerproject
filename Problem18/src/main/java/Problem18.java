import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by killerforfun on 15.03.15.
 */
public class Problem18 {
    public static void main(String[] args) {
        Problem18 p = new Problem18();
        int[][] a = p.readIntTriangle(15, "input");
        System.out.println(p.simplifyTriangle(a, 15)[0][0]);

    }

    public int[][] simplifyTriangle(int[][] inputTriangle, int currentRow) {
        for (int i = 0; i < currentRow - 1; i++) {
            inputTriangle[currentRow - 2][i] += Math.max(inputTriangle[currentRow - 1][i], inputTriangle[currentRow - 1][i + 1]);
        }
        if (currentRow > 2) {
            return simplifyTriangle(Arrays.copyOf(inputTriangle, currentRow - 1), currentRow - 1);
        }
        return Arrays.copyOf(inputTriangle, currentRow - 1);
    }

    public int[][] readIntTriangle(int rows, String inputFileName) {
        Path inputFile = Paths.get(getClass().getClassLoader().getResource(inputFileName).getFile());
        try {
            Scanner input = new Scanner(inputFile.toFile());

            int[][] a = new int[rows][rows];

            input.close();

            input = new Scanner(inputFile.toFile());
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < i + 1; j++) {
                    if (input.hasNextInt()) {
                        a[i][j] = input.nextInt();
                    }
                }
            }
            System.out.println("Read file: " + inputFile.toAbsolutePath());
            return a;
        } catch (FileNotFoundException e) {
            System.out.println("No file found in: " + inputFile.toAbsolutePath());
            return null;
        }
    }


}
